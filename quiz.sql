-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : lun. 07 sep. 2020 à 12:28
-- Version du serveur :  5.7.24
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `quiz`
--

-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `choice1` varchar(50) NOT NULL,
  `choice2` varchar(50) NOT NULL,
  `choice3` varchar(50) NOT NULL,
  `choice4` varchar(50) NOT NULL,
  `answer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `questions`
--

INSERT INTO `questions` (`id`, `question`, `choice1`, `choice2`, `choice3`, `choice4`, `answer`) VALUES
(1, 'Sam Snead is the PGA TOUR\'s all-time wins leader with 82 victories. How many does Tiger have?', '78', '79', '80', '81', '80'),
(2, 'How many times has Tiger finished second on the PGA TOUR?', '24', '58', '45', '31', '31'),
(3, 'In what percentage of his 346 starts has Tiger made the cut?', '98', '91', '86', '80', '91'),
(4, 'What is the largest winning margin of Tiger’s career?', '12', '8', '15', '10', '15'),
(5, 'Tiger is 11-1 in playoffs. His only loss came to:', 'Vijay Singh', 'Billy Mayfair', 'Ernie Els', 'Bart Bryant', 'Billy Mayfair'),
(6, 'Tiger holds the PGA TOUR record for most seasons of five-plus wins. He won five or more events in how many seasons:', '5', '6', '8', '10', '10'),
(7, 'Tiger won his first title in his __th PGA TOUR start as a pro:', '1st', '5th', '10th', '20th', '5th'),
(8, 'Who did Tiger Woods defeat in his first PGA TOUR playoff?', 'Tom Lehman', 'Phil Mickelson', 'Ed Fiori', 'Davis Love III', 'Davis Love III'),
(9, 'Tiger Woods also won THE PLAYERS during the Tiger Slam of 2000-01, giving him the five biggest titles in professional golf?\"', 'True', 'False', '', '', 'True'),
(10, 'Tiger Woods won the first of his three U.S. Junior Amateurs on a course where he would later win eight PGA TOUR titles. What course was it?', 'Firestone', 'Bay Hill', 'Torrey Pines', 'Doral', 'Bay Hill'),
(11, 'How many times has Tiger failed to win in a season when he’s made more than 12 starts?', '0', '1', '2', '3', '0'),
(12, 'Tiger has a career-high five runner-up finishes to this player. Who is it?', 'Ernie Els', 'Vijay Singh', 'Phil Mickelson', 'Jim Furyk', 'Phil Mickelson'),
(13, 'Tiger finished second in all of this player’s PGA TOUR wins:', 'Rich Beem', 'Y.E. Yang', 'Trevor Immelman', 'Bart Bryant', 'Trevor Immelman'),
(14, 'Tiger’s first PGA TOUR event was the:', '1995 Masters', '1992 Genesis Open', '1992 Farmers Insurance Open', '1993 Honda Classic', '1992 Genesis Open'),
(15, 'Where did Tiger Woods win by 15 strokes?', 'Augusta National', 'Pebble Beach', 'Bay Hill', 'Firestone', 'Pebble Beach'),
(16, 'Tiger’s career low round on the PGA TOUR is:', '59', '60', '61', '62', '61'),
(17, 'Byron Nelson holds the longest winning streak in TOUR history (11 events). What is the longest winning streak of Tiger’s career?', '5 events', '10 events', '7 events', '3 events', '7 events'),
(18, 'Tiger Woods is the only player to win THE PLAYERS in both March and May.', 'True', 'False', '', '', 'True'),
(19, 'Tiger Woods holds the record for most weeks at No. 1 in the Official World Golf Ranking. How many weeks was he No. 1?', '406 weeks', '683 weeks', '254 weeks', '927 weeks', '683 weeks'),
(20, 'How long was Tiger’s longest reign at No. 1 in the world ranking?', '281 weeks', '501 weeks', '105 weeks', '368 weeks', '281 weeks'),
(21, 'The biggest match-play win of Tiger’s career came at the 2006 World Golf Championships-Dell Technologies Match Play. He beat Stephen Ames. What was the winning margin:', '7 and 6', '8 and 7', '9 and 8', '10 and 8', '9 and 8'),
(22, 'Tiger Woods has finished runner-up to Phil Mickelson more than Phil has finished second to Tiger?', 'True', 'False', '', '', 'True'),
(23, 'Tiger Woods made a PGA TOUR record 142 consecutive cuts. Where did his streak end?', '2006 U.S. Open', '1998 Canadian Open', '2005 AT&T Byron Nelson', '2009 Open Championship', '2005 AT&T Byron Nelson'),
(24, 'Tiger Woods won the inaugural FedExCup after winning the TOUR Championship with the lowest 72-hole score of his career. What was that score?', '253', '257', '259', '260', '257'),
(25, 'How long was Tiger’s first tenure at No. 1 in the world ranking?', '1 week', '26 weeks', '50 weeks', '281 weeks', '1 week'),
(26, 'Tiger Woods won both the U.S. Amateur and NCAA Championship in 1996. He won the NCAA Championship by four strokes after shooting what score in the final round?', '65', '68', '75', '80', '80'),
(27, 'Tiger has won six tournaments five or more times. Which one of these events has he NOT won at least five times?', 'PGA Championship', 'BMW Championship', 'Memorial Tournament presented by Nationwide', 'Arnold Palmer Invitational presented by Mastercard', 'PGA Championship'),
(28, 'Tiger Woods won the 1997 Masters by 12 shots. What did he shoot on his opening nine holes of the tournament?', '30', '40', '36', '29', '40'),
(29, 'Dustin Johnson has five wins in World Golf Championships, the second-most all-time. Tiger leads the list with how many WGC wins?', '10', '12', '15', '18', '18'),
(30, 'What is Tiger’s career winning percentage?', '18%', '23%', '27%', '15%', '23%'),
(31, 'How many times has Tiger won a tournament by four or more strokes?', '8', '31', '18', '24', '24'),
(32, 'What is Tiger’s all-time Presidents Cup record?', '24-15-1', '15-24-1', '19-20-1', '20-19-1', '24-15-1'),
(33, 'When Tiger became No. 1 in the world ranking for the first time, he surpassed this player. Tiger also beat this player in singles in Tiger’s Presidents Cup debut.', 'Greg Norman', 'Nick Price', 'Ernie Els', 'Vijay Singh', 'Greg Norman'),
(34, 'Tiger has finished in the top 5 in __ percent of his PGA TOUR starts?', '25 percent', '67 percent', '33 percent', '46 percent', '46 percent'),
(35, 'Tiger has held the outright 54-hole lead (no ties) 45 times in his PGA TOUR career. How many times has he FAILED to win when holding the outright 54-hole lead?', '0', '1', '2', '3', '2'),
(36, 'How many times has Tiger led the PGA TOUR in par-5 birdie or better percentage?', '0', '10', '3', '7', '10'),
(37, 'Tiger leads the PGA TOUR career money list with $115,504,853. How large is his lead over No. 2 Phil Mickelson?', '$5 million', '$12 million', '$27 million', '$39 million', '$27 million'),
(38, 'What is Tiger’s largest final-round comeback?', '4 strokes', '5 strokes', '6 strokes', '7 strokes', '5 strokes'),
(39, 'Tiger played just six tournaments in 2008 before his season was ended by knee surgery. He finished second once. How many times did he win?', '5', '4', '2', '1', '4'),
(40, 'Tiger played 76 rounds in 2000. How many were over par?', '0', '4', '10', '12', '4'),
(41, 'How many holes-in-one has Tiger made on the PGA TOUR?', '0', '10', '3', '7', '3'),
(42, 'How many times has Tiger won a tournament by 10 or more strokes?', '2', '3', '4', '5', '4'),
(43, 'What is Tiger’s average margin of victory in his 77 stroke-play wins on the PGA TOUR?', '1 stroke', '2 strokes', '3 strokes', '4 strokes', '3 strokes');

-- --------------------------------------------------------

--
-- Structure de la table `scores`
--

CREATE TABLE `scores` (
  `id` int(11) NOT NULL,
  `score` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `scores`
--

INSERT INTO `scores` (`id`, `score`) VALUES
(7, 1),
(10, 1),
(11, 3),
(12, 3),
(13, 5),
(14, 4),
(16, 0),
(17, 0),
(18, 1),
(19, 0),
(20, 1),
(21, 1),
(22, 1),
(23, 0),
(24, 1),
(25, 0),
(26, 0),
(27, 0),
(28, 1),
(29, 0),
(30, 1),
(31, 1),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 1),
(38, 1),
(39, 0),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 2),
(48, 1),
(49, 6),
(50, 6),
(51, 7),
(52, 7);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT pour la table `scores`
--
ALTER TABLE `scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
