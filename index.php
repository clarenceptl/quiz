<?php session_start(); ?>
<?php
unset($_SESSION['score']);
unset($_SESSION['questions']);
unset($_SESSION['idQuestion']);
unset($_SESSION['check']);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/styles.css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>The Tiger Quiz</title>
</head>
<body>
    <header>
        <div class="logo">
            <a href="index.php">
                <img src="images/HB-Logo-Black - Horizontal.svg" alt="logo">
            </a>
        </div>
    </header>
    <div class="container">
        <div class="container-text">
            <h1>The Tiger Quiz</h1>
            <h2>Take this quiz and win a round of golf with the best golfer in history.</h2>
            <div>
                <a class="container-button" href="pages/questions.php">START</a>
            </div>
        </div>
        <div class="container-image">
            <img src="images/25-257623_download-tiger-woods-png-transparent-image-tiger-wood.png" alt="tiger-woods">
        </div>
    </div>
    <footer>
        <div class="footer">
            <div class="footer-message">
                <p>Hope<br/> Is not a Strategy</p>
            </div>
            <div class="footer-copyright">
                <p>All rights reserved ©hello birdie 2020</p>
                <p>made with passion for the &#128150 of the game</p>
            </div>
        </div>
    </footer>
</body>
</html>