<?php session_start(); ?>
<?php
require "../database.php";
$value=$_SESSION['score'];
$scores=[
    'score' => trim($value)
];

$prepare = $pdo->prepare('
INSERT INTO 
    scores (score) 
VALUES 
    (:score)
');
$prepare->execute($scores);
$query = $pdo->query("SELECT AVG(score) AS total FROM scores");
$avg_table= $query->fetchAll();
foreach($avg_table as $avg_value){
    $moyenne = $avg_value->total;
}
$moyenne=round($moyenne, 2);
?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/styles.css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Congratulation !</title>
</head>
<body>
    <header>
        <div class="logo">
            <a href="../index.php">
                <img src="../images/HB-Logo-Black - Horizontal.svg" alt="logo">
            </a>
        </div>
    </header>
    <div class="container">
        <div class="container-text">
            <h1>The quiz is over !</h1>
            <h2>You're score is : <?= $_SESSION['score'] ?></h2>
            <h2>The average score for all respondents is <?= $moyenne ?></h2>
            <div>
                <a  class="container-button" href="../index.php">Take again</a>
            </div>
        </div>
    </div>
    <footer>
        <div class="footer">
            <div class="footer-message">
                <p>Hope<br/> Is not a Strategy</p>
            </div>
            <div class="footer-copyright">
                <p>All rights reserved ©hello birdie 2020</p>
                <p>made with passion for the &#128150 of the game</p>
            </div>
        </div>
    </footer>
</body>
</html>