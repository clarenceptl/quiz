<?php session_start(); ?>
<?php
require "../database.php";
if(!isset($_SESSION['questions'])){
    $query = $pdo->query("SELECT * FROM questions ORDER BY RAND() LIMIT 0,10");
    $questions = $query->fetchAll();
    $_SESSION['questions']=$questions;
    $_SESSION['idQuestion']=0;
}
if(!isset($_SESSION['score'])){
    $_SESSION['score']=0;
}
$mes_questions=[];
$mes_choix1=[];
$mes_choix2=[];
$mes_choix3=[];
$mes_choix4=[];
$id_bbd=[];
$validate=[];
$i=0;
foreach($_SESSION['questions'] as $questions){
    $mes_questions[$i]=$questions->question;
    $mes_choix1[$i]=$questions->choice1;
    $mes_choix2[$i]=$questions->choice2;
    $mes_choix3[$i]=$questions->choice3;
    $mes_choix4[$i]=$questions->choice4;
    $id_bbd[$i] = $questions->id;
    $validate[$i]= $questions->answer;
    $i++;
}



$_SESSION['id_bdd']=$id_bbd;



$questions_number = $_SESSION['idQuestion']+1;

?>




<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/styles.css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>The Quiz</title>
</head>
<body>
    <header>
        <div class="logo">
            <a href="../index.php">
                <img src="../images/HB-Logo-Black - Horizontal.svg" alt="logo">
            </a>
        </div>
    </header>
    <div class="container">
        <div class="container-text">
            <div class="container-question" id="container">
                <form method="post" action="../pages/process.php">
                    <h1>Question <?=$questions_number ?></h1>
                    <h2><?= ($mes_questions[$_SESSION['idQuestion']]); ?></h2>
                    <div class="propositions">
                        <div>
                        <input type="radio" id="choice1" class="choice" onclick="return recuperCheck(this)" name="choice" value="<?= ($mes_choix1[$_SESSION['idQuestion']]);?>" >
                            <label for="choice"><?= ($mes_choix1[$_SESSION['idQuestion']]); ?></label>
                        </div>
                        <div>
                            <input type="radio" id="choice2" class="choice" onclick="return recuperCheck(this)" name="choice" value="<?= ($mes_choix2[$_SESSION['idQuestion']]);?>" >
                            <label for="choice"><?= ($mes_choix2[$_SESSION['idQuestion']]); ?></label>
                        </div>
                        <?php if (!empty($mes_choix3[$_SESSION['idQuestion']])) : ?>
                        <div>
                            <input type="radio" class="choice" id="choice3" onclick="return recuperCheck(this)" name="choice" value="<?= ($mes_choix3[$_SESSION['idQuestion']])?>" >
                            <label for="choice"><?= ($mes_choix3[$_SESSION['idQuestion']]); ?></label>
                        </div>
                        <?php endif; ?>
                        <?php if (!empty($mes_choix4[$_SESSION['idQuestion']])): ?>
                        <div>
                            <input type="radio" class="choice" id="choice4" onclick="return recuperCheck(this)" name="choice" value="<?= ($mes_choix4[$_SESSION['idQuestion']]);?>" >
                            <label for="choice"><?= ($mes_choix4[$_SESSION['idQuestion']]); ?></label>
                        </div>
                        <?php endif; ?>
                    </div>
                        
                    <input type="hidden" name="id" value="<?=  $questions_number; ?>">
                    
                    <div class="container-text-good">
                        <p>Yeah. Good answer !</p>
                    </div>
                    <div class="container-text-wrong">
                        <p>The correct answer was : <?= $validate[$_SESSION['idQuestion']] ?></p>
                    </div>
                    <div class="container-button validate">
                        <p name="validate" >Validate</p>
                    </div>
                    <button name="next" type="submit" class="container-button next" value="Submit">Next question</button>
                </form>
            </div>
        </div>
    </div>
    <footer>
        <div class="footer">
            <div class="footer-message">
                <p>Hope<br/> Is not a Strategy</p>
            </div>
            <div class="footer-copyright">
                <p>All rights reserved ©hello birdie 2020</p>
                <p>made with passion for the &#128150 of the game</p>
            </div>
        </div>
    </footer>
<script type="text/javascript">

let buttonValidate = document.querySelector(".container-button.validate")
let buttonNext = document.querySelector(".container-button.next")
let validate ='<?= $validate[$_SESSION['idQuestion']];?>';
let goodAnswer = document.querySelector(".container-text-good")
let wrongAnswer = document.querySelector(".container-text-wrong")
let propositions = document.querySelector(".propositions")
let value=[];
let choiceValue;

// let choiceChecked =[]


function recuperCheck(object) {
  if (object.checked) {
    value.push(object.value);
    buttonValidate.style.cursor = "pointer"
    buttonValidate.style.opacity = 1
    choiceValue=value.pop();//prend la derner valeur choisi
    buttonValidate.addEventListener("click", ()=> {
        propositions.style.pointerEvents= "none"
        buttonNext.style.display = "block"
        buttonValidate.style.display = "none"

        if(validate===choiceValue){
            goodAnswer.style.display = "block"
        }else{
            wrongAnswer.style.display = "block"
        }
    })
    
  }
}


</script>
</body>

</html>